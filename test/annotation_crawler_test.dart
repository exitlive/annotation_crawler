import 'dart:mirrors';

import 'package:annotation_crawler/annotation_crawler.dart';
import 'package:test/test.dart';

part 'src/definitions.dart';

void main() {
  group("annotatedDeclarations()", () {
    test(
        "if passed an instance of an annotation, should get any top level declarations with that instance",
        () {
      expect(
          annotatedDeclarations(AnnotationInstance)
              .map((decl) => decl.declaration.simpleName),
          [#foo]);
    });
    test(
        "if passed an annotation type, should get any top level declarations with an instance of that type",
        () {
      final decls = annotatedDeclarations(ClassAnnotation);

      expect(
          decls.map((decl) => decl.declaration.simpleName),
          unorderedEquals(
              <Symbol>[#ClassWithAnnotation, #SecondClassWithAnnotation]));
      expect(decls.map<dynamic>((decl) => decl.annotation),
          everyElement(const ClassAnnotation()));
    });

    test(
        "if passed an instance of an annotation, should get all declarations in the given scope with the declaration",
        () {
      final decls =
          annotatedDeclarations(AnnotationInstance, on: ClassWithMethods);
      expect(decls.map((decl) => decl.declaration.simpleName), [#bar]);
    });
    test(
        "if passed an annotation type, should get all declarations in scope with an instance of that type",
        () {
      final decls =
          annotatedDeclarations(MethodAnnotation, on: ClassWithMethods);
      expect(
          decls.map((decl) => decl.declaration.simpleName),
          unorderedEquals(<Symbol>[
            #methodWithAnnotation,
            #anotherMethodWithAnnotation,
            #variable,
            #annotatedImplementation
          ]));
      expect(decls.map<dynamic>((decl) => decl.annotation),
          everyElement(const MethodAnnotation()));
    });

    test(
        "if recursive is true, annotations on super classes and interfaces are included",
        () {
      final decls = annotatedDeclarations(MethodAnnotation,
          on: ClassWithMethods, recursive: true);
      expect(
          decls.map((decl) => decl.declaration.simpleName),
          unorderedEquals(<Symbol>[
            #methodWithAnnotation,
            #anotherMethodWithAnnotation,
            #variable,
            #annotatedImplementation,
            #interfaceMethod,
            #superclassVariable
          ]));
      expect(
          decls
              .map((decl) => decl.declaration)
              .whereType<MethodMirror>()
              .fold<bool>(true, (r, m) => r && !m.isAbstract),
          true);
    });
  });

  group('findClasses()', () {
    test("returns all classes with provided annotation class", () {
      final foundClasses = findClasses(ClassAnnotation);

      expect(foundClasses.length, equals(2));

      expect(
          foundClasses.firstWhere(
            (classMirror) => classMirror.reflectedType == ClassWithAnnotation,
          ),
          isNotNull);

      expect(
          foundClasses.firstWhere((ClassMirror classMirror) =>
              classMirror.reflectedType == SecondClassWithAnnotation),
          isNotNull);
    });
    test("doesn't return classes with unnused annotation", () {
      final foundClasses = findClasses(UnusedAnnotation);

      expect(foundClasses.length, equals(0));
    });
  });

  group('findMethodsOnInstance()', () {
    test("returns all methods provided annotation class", () {
      final instance = ClassWithMethods();

      final foundMethods = findMethodsOnInstance(instance, MethodAnnotation);

      expect(foundMethods.map((m) => m.simpleName), [
        #methodWithAnnotation,
        #anotherMethodWithAnnotation,
        #annotatedImplementation
      ]);
    });
    test("doesn't return methods with unused annotation", () {
      // ignore: unused_local_variable
      final cls = reflectClass(ClassWithMethods);

      final instance = ClassWithMethods();

      final foundMethods = findMethodsOnInstance(instance, UnusedAnnotation);

      expect(foundMethods, <dynamic>[]);
    });
  });

  group('findMethodsOnClass()', () {
    test("returns all methods provided annotation class", () {
      final foundMethods =
          findMethodsOnClass(ClassWithMethods, MethodAnnotation);

      expect(foundMethods.map((m) => m.simpleName), [
        #methodWithAnnotation,
        #anotherMethodWithAnnotation,
        #annotatedImplementation
      ]);
    });
    test("doesn't return methods with unused annotation", () {
      final foundMethods =
          findMethodsOnClass(ClassWithMethods, UnusedAnnotation);

      expect(foundMethods, <dynamic>[]);
    });
  });
}
