part of '../annotation_crawler_test.dart';

class AnnotationInstance {
  const AnnotationInstance();
}

const anno = AnnotationInstance();

class UnusedAnnotation {
  const UnusedAnnotation();
}

class ClassAnnotation {
  const ClassAnnotation();
}

class MethodAnnotation {
  const MethodAnnotation();
}

@ClassAnnotation()
class ClassWithAnnotation {}

@ClassAnnotation()
class SecondClassWithAnnotation {}

/// Just to make sure classes without annotation aren't returned.
class RandomClass {}

@anno
void foo() {}

abstract class MixinWithMethods {
  @MethodAnnotation()
  void interfaceMethod() {}

  void annotatedImplementation();
}

class SuperclassWithMethods {
  @MethodAnnotation()
  dynamic superclassVariable;
}

class ClassWithMethods extends SuperclassWithMethods with MixinWithMethods {
  @anno
  dynamic get bar => null;

  @MethodAnnotation()
  void methodWithAnnotation() {}

  @MethodAnnotation()
  void anotherMethodWithAnnotation() {}

  void methodWithoutAnnotation() {}

  @override
  @MethodAnnotation()
  annotatedImplementation() {}

  //Variables
  @MethodAnnotation()
  dynamic variable;
}
