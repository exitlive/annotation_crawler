# Annotation Crawler

Helps finding annotated declarations in a particular scope.

The main functions to use in this library are:

```dart
// Returns a List of AnnotatedDeclaration for each top level declaration that has Foo as annotation.
annotatedDeclarations(Foo);
// Does the same, but only for declarations in SomeClass
annotatedDeclarations(Foo, on: SomeClass);
```

```dart
// Returns a List of ClassMirror for each class that has Foo as annotation.
findClasses(Foo);
```

```dart
// Returns a List of MethodMirror for each method on SomeClass that has Foo as annotation.
findMethodsOnClass(SomeClass, Foo);
```

```dart
// Returns a List of MethodMirror for each method on someObject that has Foo as annotation.
findMethodsOnInstance(someObject, Foo);
```
