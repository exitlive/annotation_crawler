/// This library helps finding classes and methods with specific annotations.
library annotation_crawler;

import 'dart:collection';
import "dart:mirrors";

class AnnotatedDeclaration {
  final DeclarationMirror declaration;
  final dynamic annotation;

  AnnotatedDeclaration._(this.declaration, this.annotation);
}

/// Returns a [List] of declarations that have given annotation.
///
/// If [on] is `null`, finds declarations at the top level of any library in the current
/// [MirrorSystem], otherwise finds declarations (variables, properties, methods and constructors) declared on [on].
///
/// If [recursive] is `true` and [on] is provided, superclasses, annotated methods
/// defined on superclasses and interfaces will be included.
///
/// If an abstract method is annotated on both  a class and on the implementing class, then only the overridden
/// method is included.
///
/// eg. given the annotations
///
///     class Foo { const Foo(); }
///
///     class Bar {
///       final String name;
///       const Bar(this.name);
///     }
///
///     @Foo()
///     bar();
///
///     @Bar('Bar Name')
///     class Clazz {
///       @Foo()
///       myMethod() {}
///     }
///
/// then
///
///     final annotatedDeclaration = annotatedDeclarations(Bar).single;
///
/// is an [AnnotatedDeclaration] with [AnnotatedDeclaration.declaration] being a [ClassMirror] on `Clazz` and
/// [AnnotatedDeclaration.annotation] set to `const Bar('Bar Name')`.
///
///
/// ### Top level vs scoped declarations
///
///     // A MethodMirror on the top level method `bar`
///     annotatedDeclarations(Foo).single.declaration;
///
///     // A MethodMirror on `Clazz.myMethod`.
///     annotatedDeclarations(Foo, on: Clazz).single.declaration;
List<AnnotatedDeclaration> annotatedDeclarations(Type annotation,
    {Type? on, bool recursive = false}) {
  final annotationMirror = reflectClass(annotation);
  return UnmodifiableListView(on == null
      ? _topLevelAnnotatedDeclarations(annotationMirror)
      : _findDeclarationsOn(reflectClass(on), annotationMirror,
          recursive: recursive));
}

/// Goes through all the classes in this library, and returns those with the
/// provided class as annotation.
List<ClassMirror> findClasses(Type annotation) => UnmodifiableListView(
    _topLevelAnnotatedDeclarations(reflectClass(annotation))
        .map((annoDecl) => annoDecl.declaration)
        .whereType<ClassMirror>());

/// Returns all methods with provided annotation on passed class.
List<MethodMirror> findMethodsOnClass(Type cls, Type annotation) =>
    UnmodifiableListView(
        _findDeclarationsOn(reflectClass(cls), reflectClass(annotation))
            .map((annoDecl) => annoDecl.declaration)
            .whereType<MethodMirror>());

/// Returns all methods with provided annotation in the provided instance.
List<MethodMirror> findMethodsOnInstance(Object obj, Type annotation) =>
    findMethodsOnClass(obj.runtimeType, annotation);

Iterable<AnnotatedDeclaration> _findDeclarationsOn(
    ClassMirror cls, ClassMirror annotation,
    {bool recursive = false}) {
  _toMap(Iterable<AnnotatedDeclaration> decls) =>
      Map<Symbol, AnnotatedDeclaration>.fromIterable(decls,
          key: (dynamic decl) =>
              (decl as AnnotatedDeclaration).declaration.simpleName);

  final decls = <Symbol, AnnotatedDeclaration>{};
  if (recursive) {
    if (cls.superclass != null) {
      decls.addAll(_toMap(_findDeclarationsOn(cls.superclass!, annotation,
          recursive: recursive)));
    }
    decls.addAll(_toMap(cls.superinterfaces.expand((iface) =>
        _findDeclarationsOn(iface, annotation, recursive: recursive))));
  }

  decls.addAll(_toMap(_filterAnnotated(cls.declarations.values, annotation)));

  return decls.values;
}

Iterable<AnnotatedDeclaration> _topLevelAnnotatedDeclarations(
        ClassMirror annotation) =>
    currentMirrorSystem()
        .libraries
        .values
        .expand((lib) => _filterAnnotated(lib.declarations.values, annotation));

Iterable<AnnotatedDeclaration> _filterAnnotated(
    Iterable<DeclarationMirror> decls, ClassMirror anno) {
  AnnotatedDeclaration? annotatedWith(DeclarationMirror declMirror) {
    for (var mdata in declMirror.metadata) {
      if (mdata.type == anno) {
        return AnnotatedDeclaration._(declMirror, mdata.reflectee);
      }
    }
    return null;
  }

  return decls.map(annotatedWith).whereType<AnnotatedDeclaration>();
}
