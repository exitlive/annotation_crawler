## 2.0.0

- Null safety support

## 1.1.1

- Add `implicit-casts: false` and `implicit-dynamics: false` to analysis option to be strong mode safe

## 1.1.0

- `annotatedDeclarations` now expects a `Type` as the `on` parameter: `annotatedDeclarations(Foo, on: SomeClass);`
- Added a proper example to `example/main.dart`
- Add CI tests for formatting and analysis

## 1.0.1

- Fix README example

## 1.0.0

- Started the changelog
- Made this package dart 2.0 compatible
