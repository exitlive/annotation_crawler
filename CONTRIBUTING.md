# Contributing

We only accept merge requests that are properly formatted (with `dartfmt`), contain no warnings or errors
(check with `dartanalyzer`) and have tests for every new change.

To run all these tests, use `tool/run_tests.sh`.

Please create an issue and discuss with us before writing a merge request to avoid unnecessary work to be done.

Thanks for helping out!