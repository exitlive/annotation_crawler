#!/usr/bin/env bash

# Fail on first error
set -e

dartanalyzer --fatal-infos --fatal-warnings .

echo
echo "Checking if the code is properly formatted..."
dart format -o none --set-exit-if-changed .

echo
echo "Running tests..."
dart test